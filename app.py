from flask import Flask, jsonify, request
from flasgger import Swagger, swag_from
import pandas as pd
import joblib
from io import StringIO

app = Flask(__name__)
swagger = Swagger(app)

@app.route('/api/v1/score/file', methods=['POST'])
@swag_from({
    'tags': ['Скоринг'],
    'summary': 'Метод по оценке финансовых показателей компании',
    'description': 'Метод принимает CSV-файл, обрабатывает его данные и возвращает результат оценки модели.',
    'parameters': [
        {
            'name': 'Датасет',
            'in': 'formData',
            'type': 'file',
            'required': True,
            'description': 'CSV-файл, содержащий набор данных для оценки моделью'
        }
    ],
    'responses': {
        200: {
            'description': 'Результат оценки финансовых показателей моделью',
            'schema': {
                'type': 'object',
                'properties': {
                    'neuralModel': {
                        'type': 'object',
                        'properties': {
                            'isBankrupt': {
                                'type': 'number',
                                'format': 'int',
                                'example': 0
                            },
                            'percentageConfidence': {
                                'type': 'number',
                                'format': 'float',
                                'example': 0.95
                            }
                        }
                    }
                }
            }
        },
        400: {
            'description': 'Невалидный файл',
            'schema': {
                'type': 'object',
                'properties': {
                    'error': {
                        'type': 'string',
                        'example': 'Файл не предоставлен или неверный тип файла'
                    }
                }
            }
        }
    }
})

def get_score_file():
    file = request.files['file']
    score_neural = 0
    degree_membership_neural = 0

    if file.filename == '':
        return jsonify({"error": "No file selected"}), 400

    if file.content_type != 'text/csv':
        return jsonify({"error": "Invalid file type"}), 400

    if file:
        dataset = get_dataset(file)

        neural_model = joblib.load('data/Neural Network.pkl')
        degree_membership_neural = get_degree_membership(neural_model, dataset)
        score_neural = get_score_model(neural_model, dataset)

    return jsonify(
        {
            "neuralModel": {
                "isBankrupt": score_neural,
                "percentageConfidence": degree_membership_neural
            }
        }
    )


def get_degree_membership(model, dataset):
    prediction_degree_membership = model.predict_proba(dataset)
    max_value = max(map(max, prediction_degree_membership))
    degree_membership = max_value

    return degree_membership


def get_score_model(model, dataset):
    prediction = model.predict(dataset)
    score = int(prediction)

    return score


def get_dataset(file):
    dataset = pd.read_csv(StringIO(file.stream.read().decode("utf-8")))
    print(f"dataset = {dataset}")

    # Замена всех значений NaN на 0
    dataset = dataset.apply(pd.to_numeric, errors='coerce', downcast='float')

    scaler = joblib.load('data/scaler.pkl')
    test_dataset = pd.DataFrame(scaler.transform(dataset), index=dataset.index,
                                columns=dataset.columns)

    return test_dataset


if __name__ == '__main__':
    app.run(debug=True)